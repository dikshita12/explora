<!-- Blog Start -->
<div class="container-fluid space w_blogs_2">
	<div class="container">
		<div class="row wc_heading">
			<?php 
	        $blog_title1 = get_theme_mod('blog_title', __('Home Blog Title','explora'));
	        if ( ! empty ( $blog_title1 ) ) { ?>
        		<h1 class="section_heading explora_blog_title"><?php echo esc_html( $blog_title1 ); ?></h1>
        	<?php } ?>
		</div>
		<div class="row w-blog">
			<div class="swiper-container home-blog">
				<?php 
				$posts_count =wp_count_posts()->publish;
				
				$args = array( 'post_type' => 'post','posts_per_page' => $posts_count , 'ignore_sticky_posts' => 1); 
				
				$post_type_data = new WP_Query( $args );
				if($post_type_data->have_posts()){ ?>
					<div class="swiper-wrapper">
					 	<?php while($post_type_data->have_posts()):
							$post_type_data->the_post();  ?>
							<div class="col-md-6 swiper-slide">
								<div class="ex-blog">
									<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<?php if(has_post_thumbnail()!=''){ ?>
								
										<div class="img-thumbnail">
											<?php if(has_post_thumbnail()): ?>
											<?php $data= array('class' =>'img-responsive'); 
											   the_post_thumbnail('explora-post-thumb', $data); ?>
											   <a href="<?php echo esc_url(wp_get_attachment_url(get_post_thumbnail_id())); ?>"></a>
											<?php endif; ?>
										</div>
									<?php } ?>
									<div class="row entry-header">
										<span class="ex-author"><i class="fa fa-user"></i><a href="<?php echo esc_url(get_author_posts_url( get_the_author_meta( 'ID' ) )); ?>"><?php echo esc_html(get_the_author()); ?></a></span>
										<span class="ex-comment"><i class="fa fa-comment"></i><?php comments_popup_link( '0', '1', '%', '', '-'); ?></span>
									</div>
									<?php the_excerpt(); ?>
									<?php $read_more = get_theme_mod( 'read_more', __('Read More','explora') ) ?>
									<a class="btn ex-btn ex-link" href="<?php the_permalink(); ?>">
										<?php echo esc_html( $read_more ); ?>
									</a>
									<div class="row entry-footer">
										<?php if(get_the_category_list() != '') { ?>
										<span class="ex-category"><i class="fa fa-folder"></i>										
											<?php esc_html__(" Category",'explora');
												the_category(', '); ?>
										</span>
										<?php } ?>
										<?php if(get_the_tag_list ()!= ''){ ?>
										<span class="ex-tags"><i class="fa fa-tags"></i>
											<?php the_tags(); ?>
										</span>
										<?php } ?>
									</div>
								</div>
							</div>
						<?php endwhile; ?>
						<?php wp_reset_postdata(); ?>
					</div>
				<?php } ?>
				<div class="swiper-button-prev home-blog-prev"><i class="fa fa-arrow-left"></i></div>
				<div class="swiper-button-next home-blog-next"><i class="fa fa-arrow-right"></i></div>
			</div>
		</div>
	</div>
</div>

<!-- Blog End -->