<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
  <meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <?php wp_head(); ?>
</head>
<body <?php if(get_theme_mod('box_layout')==2) { body_class('boxed'); } else body_class(); ?>>
<?php   
if ( function_exists( 'wp_body_open' ) )
wp_body_open();
?>
<div class="wrapper">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'explora' ); ?></a>
	<!-- Header Start -->
	<header>
	<div class="container-fluid top">
		<div  class="container">
			<div class="col-md-6 col-sm-6 top-left">
				<div class="explora_social_media_head">
					<?php 
					$header_social_media_in_enabled = absint(get_theme_mod('header_social_media_in_enabled', 1));
					if ($header_social_media_in_enabled == 1) {  ?>
						<ul class="f_social">
							<?php 
							$fb_link = get_theme_mod('fb_link');
                        	if (!empty ($fb_link)) { ?>
								<li class="facebook"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
							<?php }
							$twitter_link = get_theme_mod('twitter_link');
                        	if (!empty ($twitter_link)) { ?>
								<li class="twitter"><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
							<?php } 
							$youtube_link = get_theme_mod('youtube_link');
                        	if (!empty ($youtube_link)) { ?>
								<li class="youtube"><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
							<?php } 
							$linkedin_link = get_theme_mod('linkedin_link');
	                        if (!empty ($linkedin_link)) { ?>
								<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin-in"></i></a></li>
							<?php } 
							$instagram = get_theme_mod('instagram');
                        	if (!empty ($instagram)) { ?>
                        		<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
                        	<?php } ?>
						</ul>
					<?php } ?>
				</div>
			</div>
			
			<?php 
			$explora_search = absint(get_theme_mod( 'explora_search', '1' ));
    		if ( $explora_search == "1" ) { ?>

				<div class="col-md-4 top-search-form">
					<?php echo get_search_form(); ?>	
				</div>	
			<?php } ?>
		</div>
	</div>
	<nav class="navbar navbar-default menu">
		<div class="container-fluid">
			<div class="container">
				<div class="row menu-head">
					<nav id="site-navigation" class="main-navigation navbar" role="navigation">
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>                        
							</button>
							<div claSS="logo">
								<?php if (has_custom_logo()) { the_custom_logo(); } ?>
								<?php if (display_header_text() == true) {  ?> 
									<a class="brand" href="<?php echo esc_url( home_url( '/' ) ); ?>">
										<h1><?php echo esc_html(get_bloginfo('name'));  ?></h1>
									</a>	
								<?php } ?>
								<?php if (display_header_text() == true) { ?>
									<p class="site-description"><?php bloginfo('description'); ?></p> 
								<?php } ?>
							</div>
						</div>
						<div class="collapse navbar-collapse" id="myNavbar">
							<?php  wp_nav_menu( array(
							'theme_location' => 'explora-primary-menu',
							'menu_class'     => 'nav navbar-nav navbar-right',
							'fallback_cb'    => 'explora_fallback_page_menu',
							'walker'         => new explora_nav_walker(),
							));	 ?>
						</div>
					</nav>
				</div>
			</div>
		</div>
	</nav>
</header>
<!-- Header End -->
<div id="content" class="site-content">