<div class="explora_service main-container">
    <div class="container">
		<div class="margin-100"> </div>
		<div class="container notice">
			<div class="notice-head">
				<p>	<?php esc_html_e( 'For customizing home-page, first activate the weblizar-companion plugin', 'explora' ); ?> </p>
				<?php $url = admin_url(); ?>
				<a target="_blank" href="<?php echo esc_url($url). 'plugin-install.php?tab=plugin-information&plugin=weblizar-companion&TB_iframe=true&width=772&height=877' ?>"><?php esc_html_e( 'Install Plugin','explora' ); ?></a>
			</div>
		</div>
	</div>
</div>