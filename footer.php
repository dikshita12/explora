<!-- Footer Start -->
<footer>
	<?php if ( is_active_sidebar( 'explora-footer' ) )
	{ ?>
		<div class="container-fluid w_footer">
			<div class="container">
				<?php  dynamic_sidebar( 'explora-footer' );	?>
			</div>
		</div>
	<?php } ?>
	<div class="conatainer-fluid footer-copy">
		<div class="container">
			<div class="col-md-8 col-sm-8 footer-copy-text">	
				<?php if ( explora_theme_is_companion_active() ) { ?>	
					<span class="explora_footer_text">
						<?php 
						$explora_footer_customization = get_theme_mod( 'explora_footer_customization', __('&copy; Copyright 2020. All Rights Reserved','explora') );
						echo esc_html( $explora_footer_customization ); ?>
					</span>
					<a target="_blank" rel="nofollow" href="<?php echo esc_url( get_theme_mod( 'explora_deve_link' ) ); ?>">
						<?php 
						$explora_develop_by = get_theme_mod( 'explora_develop_by' );
						echo esc_html( $explora_develop_by ); ?>
					</a>
				<?php 
		        }else{ ?>
			        <span class="explora_footer_text">
		                <?php esc_html_e('&copy; Copyright 2020. All Rights Reserved','explora'); ?>
		            </span>  
			    <?php } ?>
			</div>
			<div class="col-md-4 col-sm-4 footer-copy-social">
				<div class="explora_social_media_footer">
					<?php $footer_section_social_media_enbled = absint(get_theme_mod('footer_section_social_media_enbled', 1));
					if ($footer_section_social_media_enbled == 1) { ?>
						<ul class="f_social">
							<?php 
							$fb_link = get_theme_mod('fb_link');
                        	if (!empty ($fb_link)) { ?>
								<li class="facebook"><a href="<?php echo esc_url(get_theme_mod('fb_link')); ?>"><i class="fab fa-facebook-f"></i></a></li>
							<?php }
							$twitter_link = get_theme_mod('twitter_link');
                        	if (!empty ($twitter_link)) { ?>
								<li class="twitter"><a href="<?php echo esc_url(get_theme_mod('twitter_link')); ?>"><i class="fab fa-twitter"></i></a></li>
							<?php } 
							$youtube_link = get_theme_mod('youtube_link');
                        	if (!empty ($youtube_link)) { ?>
								<li class="youtube"><a href="<?php echo esc_url(get_theme_mod('youtube_link')); ?>"><i class="fab fa-youtube"></i></a></li>
							<?php } 
							$linkedin_link = get_theme_mod('linkedin_link');
	                        if (!empty ($linkedin_link)) { ?>
								<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('linkedin_link')); ?>"><i class="fab fa-linkedin-in"></i></a></li>
							<?php } 
							$instagram = get_theme_mod('instagram');
                        	if (!empty ($instagram)) { ?>
                        		<li class="linkedin"><a href="<?php echo esc_url(get_theme_mod('instagram')); ?>"><i class="fab fa-instagram"></i></a></li>
                        	<?php } ?>
						</ul>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
</footer>
<!-- Footer End -->
<?php if ( explora_theme_is_companion_active() ) { 
    $explora_return_top = absint(get_theme_mod( 'explora_return_top', '1' ));
    if ( $explora_return_top == "1" ) { ?>
		<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>
	<?php } 
} ?>
</div></div>
<?php  wp_footer(); ?>
</body>
</html>