<?php
/**
 * Set up the WordPress core custom header feature.
 *
 * @uses explora_header_style()
 */
function explora_custom_header_setup() {
	add_theme_support( 'custom-header', apply_filters( 'explora_custom_header_args', array(
		'default-image'          => '',
		'default-text-color'     => '000000',
		'width'                  => 1000,
		'height'                 => 250,
		'flex-height'            => true,
		'wp-head-callback'       => 'explora_header_style',
	) ) );
}
add_action( 'after_setup_theme', 'explora_custom_header_setup' );

if ( ! function_exists( 'explora_header_style' ) ) :
	/**
	 * Styles the header image and text displayed on the blog.
	 *
	 * @see explora_custom_header_setup().
	 */
	function explora_header_style() {
		$header_text_color = get_header_textcolor();
		$header_image      = get_header_image();

		wp_enqueue_style(
	        'custom-header-style1',
	        get_template_directory_uri() . '/css/custom-header-style.css'
	    );

	    $custom_css = "";

	    if ( ! display_header_text() ) {
	    	$custom_css .= "
	            .logo h1,.logo h1:hover {
				color: rgba(241, 241, 241, 0);
				position:absolute;
				clip: rect(1px 1px 1px 1px);
				}
				.logo p {
				color: rgba(241, 241, 241, 0);
				position:absolute;
				clip: rect(1px 1px 1px 1px);
				}";
	    } else {
	    	$custom_css .= ".logo h1, .logo p {
				color: #".esc_attr( $header_text_color )."!important;
			}";
	    }

	    if ( has_header_image() ) { 
	    	$custom_css .= "header {
	    		background-image: url(".$header_image.");
	    		background-repeat: no-repeat;
    			background-size: cover;
			}";
	    } 

	    wp_add_inline_style( 'custom-header-style1', $custom_css );			

		// If we get this far, we have custom styles. Let's do this.
	} 
endif; 