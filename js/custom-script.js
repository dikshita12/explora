jQuery(document).ready(function() {
	"use strict";
	jQuery('.nav li.dropdown').find('.caret').each(function(){
		jQuery(this).on('click', function(){
			if( jQuery(window).width() < 768) {
				jQuery(this).parent().next().slideToggle();
			}
			return false;
		});
	});
	
/* Slider 1 */
	var swiper = new Swiper('.explora_slider', {
		nextButton: '.swiper-button-next5',
        prevButton: '.swiper-button-prev5',
        slidesPerView: '1',
        paginationClickable: true,
        spaceBetween: 10,
		autoplay:3000,
		loop:false		
    });
/* Slider 1 */

/* Portfolio 1 */
	var swiper = new Swiper('.port-slider', {
		//nextButton: '.swiper-button-next5',
       // prevButton: '.swiper-button-prev5',
        slidesPerView: 4,
        paginationClickable: true,
        spaceBetween: 10,
		autoplay:3000,
		loop:false		
    });
/* Portfolio 1 */
	
	/* for scroll arrow */
	 var amountScrolled = 300;

	jQuery(window).scroll(function() {
		if ( jQuery(window).scrollTop() > amountScrolled ) {
			jQuery('a.back-to-top').fadeIn('slow');
		} else {
			jQuery('a.back-to-top').fadeOut('slow');
		}
	});
	
	jQuery('a.back-to-top').click(function() {
		jQuery('html, body').animate({
			scrollTop: 100
		}, 700);
		return false;
	});	
});

/* for menu in responsive */
jQuery(document).ready(function() {
	
	/* photobox */
	jQuery('.blog_gallery').photobox('.photobox_a');
	jQuery('.blog_gallery').photobox('.photobox_a:first', { thumbs:false, time:0 }, imageLoaded);
	function imageLoaded(){
		console.log('image has been loaded...');
	}
});

jQuery(document).ready(function() {
		  jQuery('.owl-carousel').owlCarousel({
			loop: true,
			margin: 10,
			autoplay:true,
			autoplayTimeout:1000,
			navigation: true,
        navigationText: [
       "<i class='fa fa-angle-left icon-white'></i>",
       "<i class='fa fa-angle-right icon-white'></i>"
       ],
			responsiveClass: true,
			
			responsive: {
			  0: {
				items: 1,
				nav: true
			  },
			  600: {
				items: 3,
				nav: false
			  },
			  1000: {
				items: 1,
				nav: true,
				loop: false,
				margin: 20
			  }
			}
		  })
		})	