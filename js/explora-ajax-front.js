var window, document, jQuery;
(function (jQuery) {
    "use strict";

   if ( ajax_admin.ajax_data.sticky_header == true ) {

        jQuery(document).ready(function() {
            jQuery(window).scroll(function () {
            if( jQuery(window).width() > 768) {
                if (jQuery(this).scrollTop() > 100) {
                jQuery('header').addClass('sticky-head');
                }
                else {
            jQuery('header').removeClass('sticky-head');
            }
            }
                else {
                if (jQuery(this).scrollTop() > 100) {
                    jQuery('header').addClass('sticky-head');
                }else {
            jQuery('header').removeClass('sticky-head');
            }
                }               
            });

        });
    }

    var swiper = new Swiper('.home-blog', {
        nextButton: '.home-blog-next',
        prevButton: '.home-blog-prev',
        slidesPerView: ajax_admin.ajax_data.blog_preview,
        paginationClickable: true,
        //spaceBetween: 20,
        autoplay:ajax_admin.ajax_data.autoplay ,
        //loop:true,
        speed: 3000,
        breakpoints: {
            1024: {
                slidesPerView: 3,
                spaceBetween: 10
            },
            768: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            640: {
                slidesPerView: 2,
                spaceBetween: 10
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 10
            }
        }
    });

    var swiper = new Swiper('.explora_slider', {
        nextButton: '.swiper-button-next5',
        prevButton: '.swiper-button-prev5',
        slidesPerView: '1',
        paginationClickable: true,
        spaceBetween: 10,
        autoplay: ajax_admin.ajax_data.slider_speed,
        loop:true       
    });

})(jQuery);