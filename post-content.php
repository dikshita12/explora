<div class="row w_blog_post">
    <div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
        <?php if(has_post_thumbnail()): ?>
            <div class="img-thumbnail">
                <?php $data= array('class' =>'img-responsive'); 
                the_post_thumbnail('explora-post-thumb', $data); ?>
                <div class="overlay">
                    <a class="photobox_a" href="<?php echo esc_url(wp_get_attachment_url(get_post_thumbnail_id())); ?>"><span class="fa fa-search icon"></span></a>
    				<?php if(!is_singular()) { ?>
                    <a href="<?php the_permalink(); ?>"><span class="fa fa-chain icon"></span></a>
    				<?php } ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="col-md-12 w_post_desc">
            <h2><?php the_title(); ?></h2>
            <span><i class="fa fa-calendar"></i><?php the_date(); ?> </span>
			
			<?php if(get_the_category_list() != '') { ?>
			<span class="ex-category"><i class="fa fa-folder"></i>										
				<?php __("Category ",'explora'); the_category(' '); ?>
			</span>
			<?php } ?>
			
            <?php if(get_the_tag_list() != '') { ?>
                <span> <i class="fa fa-tag"></i><?php the_tags( ' ', ' '); ?>
                </span>
             <?php } 
		 
            if(!is_single() && !is_page()){ 
                the_excerpt(); ?>
                <a href="<?php the_permalink(); ?>" class="w_edit"><?php esc_html_e('Read More','explora'); ?></a>
                <?php }else{
                the_content();
                $defaults = array(
                    'before'           => '<p>' . __( 'Pages:', 'explora' ),
                    'after'            => '</p>',
                    'link_before'      => '',
                    'link_after'       => '',
                    'next_or_number'   => 'number',
                    'separator'        => ' ',
                    'nextpagelink'     => __( 'Next page', 'explora'),
                    'previouspagelink' => __( 'Previous page', 'explora' ),
                    'pagelink'         => '%',
                    'echo'             => 1
                );
             
                wp_link_pages( $defaults );
            }?>            
        </div>
    </div>
</div>