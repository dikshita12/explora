=== Explora ===
Contributors: weblizar
Tags: blog, custom-header, custom-logo, custom-menu, e-commerce, featured-images, footer-widgets, full-width-template, right-sidebar, theme-options, threaded-comments, two-columns, one-column, portfolio, sticky-post, translation-ready
Requires at least: 4.9
Tested up to: 5.5
Stable tag: 1.6.3
License: GNU General Public License v2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Explora is a free WordPress theme best for multipurpose site. Easy to customize.

== Description ==
Explora is a multipurpose business corporate theme. Beautiful design with bootstrap and responsive that support woocommerce with blog left and blog right sidebar. Explora is a really simple theme, straightforward typography is readable on a wide variety of screen sizes, and suitable for multiple languages. We designed it using a mobile-first approach which means that your content takes center-stage, regardless of whether your visitors arrive by smartphone, tablet, laptop, or desktop computer. This is what makes the theme fully responsive with eye catchy effects.

<a href="https://wordpress.org/themes/explora/">Discover more about the theme</a>.

== Frequently Asked Questions ==

= Where can i raise the theme issue? =

Please drop your issues here <a href="https://wordpress.org/support/theme/explora"> we'll try to triage issues reported on the theme forum, you'll get a faster response.

= Where can I read more about Explora? =

<a href="https://wordpress.org/themes/explora/">Theme detail page</a>

-------------------------------------------------------------------------------------------------
License and Copyrights for Resources used in Explora WordPress Theme
-------------------------------------------------------------------------------------------------

1) Bootstrap
=================================================================================================
Bootstrap v3.3.5 (http://getbootstrap.com)
Copyright 2011-2015 Twitter, Inc.
Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

Bootstrap v3.1.1 (http://getbootstrap.com)
Copyright 2011-2014 Twitter, Inc.
Licensed under MIT (https://github.com/twbs/bootstrap/blob/master/LICENSE)

2) Font Awesome
=================================================================================================
Font Awesome Free 5.11.2 by @fontawesome - https://fontawesome.com
License - https://fontawesome.com/license/free (Icons: CC BY 4.0, Fonts: SIL OFL 1.1, Code: MIT License)

3) Swiper
=================================================================================================
Swiper 3.4.2
http://www.idangero.us/swiper/
Copyright 2017, Vladimir Kharlampidi
Licensed under MIT

4) Animate
=================================================================================================
animate - http://daneden.me/animate
Version - 3.5.1
Licensed under the MIT license - http://opensource.org/licenses/MIT
Copyright (c) 2016 Daniel Eden

5) Owl Carousel
=================================================================================================
Owl Carousel v2.2.1
Copyright 2013-2017 David Deutsch
Licensed under MIT

6) Photobox
=================================================================================================
photobox v1.9.9
(c) 2013 Yair Even Or <http://dropthebit.com>
MIT-style license.

== Screenshots ==

Slider image--
Source:https://pxhere.com/en/photo/1165642

Copyright 2019, Pxhere
https://pxhere.com
Pxhere provides images under license
license: https://pxhere.com/en/license

== Changelog ==

Version: 1.6.3
* Remove admin notice
* Add Accessebility
* Update Font-Awesome
* Update according to new guidline

Version: 1.6.2
* Remove Inline CSS & Js
* Create home template
* Screenshot updated
* Minor CSS corrections.
* Changes in theme info notice.

Version: 1.6.1[28/11/2019]
***Accessibility Ready

Version: 1.6[29/05/2019]
1. FA- ICONS updated.

Version: 1.5[23/03/2019]
1. New Post template Added

= 1.4.9 =
* Readme.text file changed as per new rule.
* Minor changes in theme info notice.
* Change log updated for customizer.
* Excerpt option added for home blog.

= 1.4.8 =
* Welcome page added.

= 1.4.7 =
* Rating option added on admin panel.
* boxed layout option added.

= 1.4.6 =
* Go Explora premium option added on admin panel.

= 1.4.5 =
* Pro Themes & Plugin page updated.

= 1.4.4 =
* sticky header option added.
* minor changes in theme options.
* Remove expire coupon code.

= 1.4.3 =
* issues fixed for header image.
* Search form added for home page.

= 1.4.2 =
* Autoplay option added in blog options.
* Category optoin added in blog options.
* Read More button text option added.
* Dismiss option added in rating banner.

= 1.4.1 =
* screenshot updated.

= 1.4 =
* One more slider added for home page.
* Pro theme and plugin page update.

= 1.3 =
* Added Icon Picker.  
* Added WP Editor.
* Added Section Drag-Drop. 
* Added 5-Star Rating Option. 
* Quick Edit Path Locator.

= 1.2.4 =
* Upsell Link added.

= 1.2.3 =
* Screenshot png image size manage.

= 1.2.2 =
* Fixed issue ticket url : https://themes.trac.wordpress.org/ticket/39783#comment:12

= 1.2.1 =
* Fixed issue ticket url : https://themes.trac.wordpress.org/ticket/39783#comment:9

= 1.2 =
* Fixed issue ticket url : https://themes.trac.wordpress.org/ticket/39783#comment:7

= 1.1 =
* Fixed issue ticket url : https://themes.trac.wordpress.org/ticket/39783#comment:4
* Screenshot change 

= 1.0 =
* released.