<?php
/** Theme Name    : Explora
 * Theme Core Functions and Codes
 */

/* Get the plugin */
if ( ! function_exists( 'explora_theme_is_companion_active' ) ) {
    function explora_theme_is_companion_active() {
        require_once( ABSPATH . 'wp-admin/includes/plugin.php' );
        if ( is_plugin_active(  'weblizar-companion/weblizar-companion.php' ) ) {
            return true;
        } else {
            return false;
        }
    }
}

require( get_template_directory(). '/core/menu/default_menu_walker.php' );
require( get_template_directory(). '/core/menu/explora_nav_walker.php' );
require( get_template_directory() . '/comment-function.php' );
require get_template_directory() . '/core/custom-header.php';
require(get_template_directory() . '/class-tgm-plugin-activation.php');

	/*After Theme Setup*/
	function explora_head_setup(){

		add_theme_support( 'title-tag' );

		add_theme_support( 'woocommerce' );
		add_theme_support( 'wc-product-gallery-zoom' );
	    add_theme_support( 'wc-product-gallery-lightbox' );
	    add_theme_support( 'wc-product-gallery-slider' );

	    global $content_width;
		if ( ! isset( $content_width ) ) $content_width = 900;
		
		/*
	     * Make theme available for translation.
	     * Translations can be filed in the /languages/ directory.
	     * If you're building a theme based on Theme Palace, use a find and replace
	     * to change 'explora' to the name of your theme in all the template files.
	    */
		load_theme_textdomain( 'explora'); 
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails');

		/* Gutenberg */
		add_theme_support( 'wp-block-styles' );
		add_theme_support( 'align-wide' );

		/* Add editor style. */
		add_theme_support( 'editor-styles' );
		add_theme_support( 'dark-editor-style' );

		add_theme_support( 'custom-logo', array( 
			'height'=> 80, 
			'width' => 100, 
			'flex-height' => true, 
			'flex-width'  => true,
			'header-text' => array( 'site-title', 'site-description' ),
		) );

		// for register menu
		register_nav_menu( 'explora-primary-menu', __( 'Primary Menu', 'explora' ) );

		add_theme_support( 'customize-selective-refresh-widgets' );
		
		/** * Registers an editor stylesheet for the theme. ***/
		function explora_theme_editor_styles() {
			add_editor_style( 'css/custom-editor-style.css' );
		}
		add_action( 'admin_init', 'explora_theme_editor_styles' );

		$args = array('default-color' => '#fff',);
		add_theme_support('custom-background', $args);
		
	}
	add_action( 'after_setup_theme', 'explora_head_setup' );

	/*Enqueue Styles And JS*/
	function explora_theme_style(){
		//style
		wp_enqueue_style('bootstrap',get_template_directory_uri() . '/css/bootstrap/css/bootstrap.css');
		wp_enqueue_style('fontawesome-all', get_template_directory_uri() . '/css/font-awesome-5.11.2/css/all.css');
		wp_enqueue_style('explora-swiper-css',get_template_directory_uri() . '/css/swiper.css');
		wp_enqueue_style('explora-animate-min-css',get_template_directory_uri() . '/css/animate.css');
		wp_enqueue_style('explora-photobox',get_template_directory_uri() . '/css/photobox.css');
		wp_enqueue_style('explora-owl',get_template_directory_uri() . '/css/owl.carousel.css');
        wp_enqueue_style('explora-media-Responsive',get_template_directory_uri() . '/css/media-screen.css');
		wp_enqueue_style('explora-main-css',get_template_directory_uri() . '/style.css');
		//JS
		
		wp_enqueue_script('jquery-bootstrap',get_template_directory_uri().'/css/bootstrap/js/bootstrap.js', array( 'jquery' ), true, true);
		wp_enqueue_script( 'explora-navigation', get_template_directory_uri() . '/js/navigation.js', array('jquery'), true, true );
		wp_enqueue_script('jquery-photobox',get_template_directory_uri().'/js/jquery.photobox.js', array( 'jquery' ), true, true);
		wp_enqueue_script('explora-owl-js',get_template_directory_uri().'/js/owl.carousel.js', array( 'jquery' ), true, true);
		wp_enqueue_script('explora-swiper-js',get_template_directory_uri().'/js/swiper.js', array( 'jquery' ), true, true);
		wp_enqueue_script('explora-custom-script',get_template_directory_uri().'/js/custom-script.js', array( 'jquery' ), true, true);

		$sticky_header = absint(get_theme_mod( 'sticky_header', '1' ));
		if ( $sticky_header =='1' ) {
	        $sticky_header = true;
	    } else {
	        $sticky_header = false;
	    }
	    
	    $autoplay = absint(get_theme_mod( 'autoplay', '1' ));
		if ( $autoplay =='1' ) {
	        $autoplay = true;
	    } else {
	        $autoplay = false;
	    }

		$ajax_data = array(
            'sticky_header' => $sticky_header,
            'autoplay' => $autoplay,
            'blog_preview' => get_theme_mod('blog_preview'),
            'slider_speed' => get_theme_mod('slider_speed'),
        );

        wp_enqueue_script( 'explora-ajax-front', get_template_directory_uri() . '/js/explora-ajax-front.js', array( 'jquery' ), true, true );
        wp_localize_script( 'explora-ajax-front', 'ajax_admin', array(
                'ajax_url'    => admin_url( 'admin-ajax.php' ),
                'admin_nonce' => wp_create_nonce( 'admin_ajax_nonce' ),
                'ajax_data'   => $ajax_data,
        ) );
		
		if ( is_singular() ) wp_enqueue_script( "comment-reply" );
	}
	add_action( 'wp_enqueue_scripts', 'explora_theme_style' );

	// For Post thumbnail
	if ( function_exists( 'add_image_size' ) ) add_theme_support( 'post-thumbnails' );

		if ( function_exists( 'add_image_size' ) ) { 
			add_image_size ('explora-post-thumb', 400, 300,true);
		}

	function explora_custom_excerpt_length( $length ) {
		return 20;
	}
	add_filter( 'excerpt_length', 'explora_custom_excerpt_length', 999 );

	//For Excerpt content
	function explora_excerpt_more( $more ) {
		return '.';
	}
	add_filter('excerpt_more', 'explora_excerpt_more');

	/* Explora widget area */
	add_action( 'widgets_init', 'explora_widgets_init');
	function explora_widgets_init() {
	/*sidebar*/
	register_sidebar( array(
			'name' => __( 'Sidebar', 'explora' ),
			'id' => 'explora-sidebar',
			'description' => __( 'The Sidebar Widget Area', 'explora' ),
			'before_widget' => '<div id="%1$s" class="row w_sidebar %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<h2>',
			'after_title' => '</h2>'
		) );

	register_sidebar( array(
			'name' => __( 'Footer Widget Area', 'explora' ),
			'id' => 'explora-footer',
			'description' => __( 'Footer Widget Area', 'explora' ),
			'before_widget' => '<div id="%1$s" class="col-md-3 col-sm-6 footer-widget %2$s">',
			'after_widget' => '</div>',
			'before_title' => '<div class="col-md-12 widget-heading"><h1>',
			'after_title' => '</h1></div>',
		) );             
	}

	//Plugin Recommend
	add_action('tgmpa_register', 'explora_plugin_recommend');

	function explora_plugin_recommend()
	{
	    $plugins = array(
	        array(
	            'name' => esc_html__('Weblizar Companion','explora'),
	            'slug' => 'weblizar-companion',
	            'required' => false,
	        ),
	    );

	    tgmpa($plugins);
	}