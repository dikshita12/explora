<?php get_header(); ?>
<div class="container-fluid w_breadcum">
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <ul class="explora-bredcum">
            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home', 'explora') ?></a></li>
            <li></li> <li><?php the_title(); ?></li>
        </ul>
    </div>
</div>
<div class="conatainer-fluid space w_blog">
	<div class="container">
		<div class="col-md-8 right-side blog_gallery">
			<?php if ( have_posts()) : while ( have_posts() ) : the_post(); 
				get_template_part('post','content'); 
				comments_template( '', true ); 
				endwhile;
			endif; ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>