<?php get_header(); ?>
<div class="container-fluid w_breadcum">
    <div class="container">
        <h1><?php the_title(); ?></h1>
        <ul class="explora-bredcum">
            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home', 'explora') ?></a></li>
            <li> </li> <li><?php the_title(); ?></li>
        </ul>
    </div>
</div>
<div class="conatainer-fluid space w_blog">
	<div class="container">
		<div class="col-md-8 right-side blog_gallery">
		<?php the_post();
			get_template_part('post','content'); ?>
			<div class="explora_blog_pagination">
				<div class="explora_blog_pagi">
					<p class="single-next"><?php previous_post_link('%link'); ?></p>	
					<p class="single-prev"> <?php next_post_link('%link'); ?></p>
				</div>
			</div>
			<?php 
            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif; ?>
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>