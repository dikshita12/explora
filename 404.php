<?php get_header(); ?>
<!-- BreadCum -->
<div class="container-fluid w_breadcum">
    <div class="container">
        <h1><?php esc_html_e('404 Error!','explora'); ?></h1>
        <ul class="explora-bredcum">
            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home', 'explora') ?></a></li>
            <li></li> <li><?php esc_html_e('404 error','explora'); ?></li>
        </ul>
    </div>
</div>
<!-- BreadCum -->
<div class="conatainer-fluid space w_blog">
	<div class="container">
		<div class="col-md-12 right-side blog_gallery">
		<div class="row error-page">
			<h1><?php number_format_i18n('40','explora'); ?><span class="grey"><?php number_format_i18n('4','explora'); ?></span></h1>
			<h2><span class="fa fa-exclamation-circle"></span> <?php esc_html_e('ERROR','explora'); ?></h2>
			<h3><?php esc_html_e('Page cannot be found','explora'); ?></h3>
			<p><?php esc_html_e('The Page you requested is not be found. This could be spelling error in the url.','explora'); ?></p>
			<a class="btn" href="<?php echo esc_url(home_url( '/' )); ?>"><?php esc_html_e('Go back to homepage','explora'); ?></a>
		</div>
		</div>
	</div>
</div>
<?php get_footer(); ?>