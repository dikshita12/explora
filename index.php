<?php get_header();  ?>
<div class="container-fluid w_breadcum">
    <div class="container">
        <h1><?php esc_html_e('Home','explora'); ?></h1>
        <ul class="explora-bredcum">
            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home', 'explora') ?></a></li>
        </ul>
    </div>
</div>
<!-- Blog  With Right Side bar Start -->
<div class="conatainer-fluid space w_blog">
	<div class="container">
		<div class="col-md-8 right-side blog_gallery">
		<?php if ( have_posts()){ 
				while ( have_posts() ): the_post();
					get_template_part('post','content');
				endwhile;
				} else { get_template_part('no','content');	} ?>
				<div class="explora_blog_pagination">
					<div class="explora_blog_pagi">
						<?php the_posts_pagination(); ?>
					</div>
				</div>
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>
<!-- Blog  End -->
<?php get_footer(); ?>