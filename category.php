<?php get_header(); ?>
<!-- BreadCum -->
<div class="container-fluid w_breadcum">
    <div class="container">
        <h1><?php  /* translators: %s: category name */
		printf( esc_html__( 'Category Archives: %s', 'explora' ), '<span>' . single_cat_title( '', false ) . '</span>' ); ?></h1>
        <ul class="explora-bredcum">
            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home', 'explora') ?></a></li>
            <li></li> <li><?php printf( '<span>' . single_cat_title( '', false ) . '</span>' ); ?></li>
        </ul>
    </div>
</div>
<!-- BreadCum -->
<div class="conatainer-fluid space w_blog">
	<div class="container">
		<div class="col-md-8 right-side blog_gallery">
		<?php if ( have_posts()){ 
				while ( have_posts() ): the_post();
					get_template_part('post','content');
				endwhile;
				} else {
					get_template_part('no','content');
				} ?>
			<div class="explora_blog_pagination">
				<div class="explora_blog_pagi">
					<?php the_posts_pagination(); ?>
				</div>
			</div>
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>