<?php if ( post_password_required() ) : ?>
	<p><?php esc_html_e( 'This post is password protected. Enter the password to view any comments.', 'explora' ); ?></p>
	<?php return; endif; ?>
    <?php if ( have_comments() ) : ?>
	<div class="row w_comment">		
	<h2><?php echo esc_html(comments_number(__('No Comments','explora'), __('1 Comment','explora'), __('% Comments','explora'))); ?></h2>
	<?php wp_list_comments( array( 'callback' => 'explora_comment' ) ); ?>		
	<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : ?>
		<nav id="row blog-pagination">
			<h1 class="assistive-text"><?php esc_html_e( 'Comment navigation', 'explora' ); ?></h1>
			<div class="previous"><?php previous_comments_link( __( '&larr; Older Comments', 'explora' ) ); ?></div>
			<div class="next"><?php next_comments_link( __( 'Newer Comments &rarr;', 'explora' ) ); ?></div>
		</nav>
		<?php endif;  ?>
	</div>		
	<?php endif; ?>
	
<?php if ( comments_open() ) : ?>
	<div class="col-md-12 blog-feedback">
	<?php $fields=array(
		'author' => '<div class="form-group col-md-6"><label for="name">'. __( 'Name','explora').'<small>*</small></label><input name="author" id="name" type="text" id="exampleInputEmail1" class="form-control"></div>',
		'email' => '<div class="form-group col-md-6"><label for="email">'. __( 'Email','explora').'<small>*</small></label><input  name="email" id="email" type="text" class="form-control"></div>',
	);
	function explora_fields($fields) { 
		return $fields;
	}
	add_filter('wl_comment_form_default_fields','explora_fields');
		$defaults = array(
		'fields'=> apply_filters( 'wl_comment_form_default_fields', $fields ),
		'comment_field'=> '<div class="form-group col-md-12"><label for="message">'.__('Message','explora').'</label>
		<textarea id="comment" name="comment" class="form-control" rows="5"></textarea></div>',		
		'logged_in_as' => '<p class="logged-in-as">' . __( "Logged in as ",'explora' ).'<a href="'. esc_url(admin_url( 'profile.php' )).'">'.$user_identity.'</a>'. '<a href="'. esc_url(wp_logout_url( esc_url(get_permalink()) )).'" title="'.esc_attr_e("Log out of this account",'explora').'">'.__(" Log out?",'explora').'</a>' . '</p>',
		/* translators: %s: reply to name */
		'title_reply_to' => __( 'Leave Your comments Here to %s','explora'),
		'class_submit' => 'btn',
		'label_submit'=>__( 'Post Comment','explora'),
		'comment_notes_before'=> '',
		'comment_notes_after'=>'',
		'title_reply'=> '<h3>'.__('Leave Your Comment Here','explora').'</h3>',		
		'role_form'=> 'form',		
		);
		comment_form($defaults); ?>		
		
</div>
<?php endif; // If registration required and not logged in ?>