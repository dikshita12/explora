<?php
// Template Name: Home Page
get_header(); 

if ( explora_theme_is_companion_active() ) {

	get_template_part('sections/home','slider1');

	?>
	<div class="container">
		<?php
		//****** get home callout ********
		if($sections = json_decode(get_theme_mod('home_reorder'),true)) {
			foreach ($sections as $section) {
				$data = "home_".$section;
				$data1 = absint(get_theme_mod($data, 1));
				if ($data1 == "1") {
					get_template_part('sections/explora', $section);
				}
			}
		} else {
			get_template_part('sections/explora', 'services');
	        get_template_part('sections/explora', 'portfolio');

	        $blog_home = absint(get_theme_mod('blog_home', 1));

			if ($blog_home == 1) {
				get_template_part('sections/explora','blog');
			}
		}
		?>
	</div>
<?php
} else { 
    get_template_part( 'plugin', 'content' );
}
 
get_footer(); 