<?php 
// code for comment
if ( ! function_exists( 'explora_comment' ) ) :
function explora_comment( $comment, $args, $depth ) 
{	
	//get theme data
	global $comment_data;
	//translations
	$leave_reply = $comment_data['translation_reply_to_coment'] ? $comment_data['translation_reply_to_coment'] : 
	'<i class="fa fa-mail-reply"></i> '.__('Reply','explora'); ?>
	<div class="col-xs-12 comment-details">
		<div class="col-xs-12 comment-detail">
			<a class="col-xs-2 comments-pics"><?php echo wp_kses_post( get_avatar($comment,$size = '80')); ?></a>
			<div class="col-xs-10 comments-text">
				<h3><?php esc_html(comment_author());?>
				<span>
				<?php if ( ('d M  y') == get_option( 'date_format' ) ) : ?>				
				<?php esc_html(comment_date('F j, Y'));?>
				<?php else : ?>
				<?php esc_html(comment_date()); ?>
				<?php endif; ?>
				<?php esc_html_e('at','explora');?>&nbsp;<?php esc_html(comment_time('g:i a')); ?>
				</span></h3>
				<?php esc_html(comment_text()) ; ?>				
				<div class="explora-reply_text">
				<?php esc_url( comment_reply_link(array_merge( $args, array('reply_text' => $leave_reply,'depth' => $depth, 'max_depth' => $args['max_depth']))) )?>
				</div>				
				<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php esc_html_e( 'Your comment is awaiting moderation.', 'explora' ); ?></em>
				<br/>
				<?php endif; ?>
			</div>							
		</div>	
	</div><?php
}
endif;
?>