<?php get_header(); ?>
<!-- BreadCum -->
<div class="container-fluid w_breadcum">
    <div class="container">
        <?php if ( have_posts()): ?>
        	<?php the_archive_title( '<h1 class="page-title">', '</h1>' );
        endif; ?>
        <ul class="explora-bredcum">
            <li><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><?php esc_html_e('Home', 'explora'); ?></a></li>
            <li></li>
            <li><?php the_date(); ?>
            </li>
        </ul>
    </div>
</div>
<!-- BreadCum -->
<div class="conatainer-fluid space w_blog">
	<div class="container">
		<div class="col-md-8 right-side blog_gallery">
		<?php if ( have_posts()){ 
				while ( have_posts() ): the_post();
					get_template_part('post','content');
				endwhile;
				}else{
					get_template_part('no','content');
				} ?>
			<div class="explora_blog_pagination">
				<div class="explora_blog_pagi">
					<?php the_posts_pagination(); ?>
				</div>
			</div>
		</div>
		<?php get_sidebar(); ?>
	</div>
</div>
<?php get_footer(); ?>