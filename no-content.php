<div class="row error-page">
	<h2><span class="fa fa-exclamation-circle"></span><?php esc_html_e('ERROR','explora'); ?></h2>
	<h3><?php esc_html_e('No Posts Found','explora'); ?></h3>
	<a class="btn" href="<?php echo esc_url(home_url( '/' )); ?>"><?php esc_html_e('Go back to homepage','explora'); ?></a>
</div>